import { Button, TextField, Typography } from "@mui/material";
import { useState } from "react";

export const EditableText = ({ value }: { value: string}) => {
  const [editing, setEditing] = useState(false);

  return (
    <>
      {!editing && (
        <Typography onClick={() => setEditing(!editing)}>
          {value}
        </Typography>
      )}
      {editing && (
        <>
          <TextField variant="standard" value={value} onChange={(event) => console.log(event.target.value)} />
          <Button onClick={() => console.log('saved')}>Save</Button>
          <Button onClick={() => setEditing(false)}>X</Button>
        </>
      )}
    </>
  );
};